# Sermon Extractor

_Extracting sermons from audio files_

### Install
Make sure you have these packages:
```bash
sudo apt-get install libsndfile1        // Optional, necessary on errors
sudo apt-get install ffmpeg
```


### Usage
See:
```bash
sermonExtractorMain.py -h
```

