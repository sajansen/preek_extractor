#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='sermonExtractor',
  version='1.0',
  description='Extracting sermons speech from audio files',
  package_dir={'': 'src'},
  packages=find_packages('src'),
  include_package_data=True,
  install_requires=[
    'cffi==1.12.3',
    'numpy==1.16.4',
    'PySoundFile==0.9.0.post1',
    'matplotlib==3.1.0',
    'pydub==0.23.1',
  ])
