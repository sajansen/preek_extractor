import logging
import os
import resource

import gc
import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
from sermonExtractor import fileConverter

logger = logging.getLogger(__name__)


class SermonExtractor:

  def __init__(self):
    # SETTINGS
    # Allow up to X seconds before the real speech starts
    self.output_begin_padding = 1
    # Allow up to X seconds after the real speech ends
    self.output_end_padding = 3

    # Minimum speech length in seconds. Will be used to check if the speech found is of valid length.
    self.min_speech_length = 17 * 60

    # TECHNICAL SETTIGNS
    # Averaging window size in seconds for finding difference between audio parts
    self.sampling_window_size = 3

    # If the difference in sound will be more than this threshold, it will be seen as a different fragment. Adjust
    # this value if the speech fragment is too short or if there is a lot of music in the fragment.
    self.sound_difference_threshold = 0.35

    # If the original sound signal is below this threshold (amplitude), it is considered as a silence. Adjust this
    # threshold if music is in the output fragment at the begin or end, in order to remove the music.
    self.first_silence_threshold = 0.02
    # If the original sound signal is above this threshold (amplitude), it is considered as a sound. Adjust this
    # threshold if a part of the speech is missing at the begin or end, or if there is a too long silence at the
    # begin or end.
    self.first_sound_threshold = 0.027

    # DEBUG SETTINGS
    # Enable plots which shows the sound track, found sections and the begin/end of the speech fragment. This makes
    # the converting process slower.
    self.enable_plots = False
    # Save the plots as an image file.
    self.save_plots = False

    # Class properties
    self.title = None
    self.file_format = '.wav'
    self.dataset = []
    self.speech_dataset = []
    self.samplerate = None
    self.output_files = []
    self.output_dir = '.'

  def read(self, input_file):
    logger.info("Reading file '%s'", input_file)
    self.dataset, samplerate = sf.read(input_file)

    if self.dataset is None or self.dataset.size == 0:
      raise Exception("Couldn't read sound from file.")

    self.samplerate = int(samplerate)
    self.title, self.file_format = os.path.splitext(input_file)
    self.title = os.path.basename(input_file)

  def convert(self):
    logger.info("Converting sound to speech: ")
    self.create_output_dir()

    sample_window = self.samplerate * self.sampling_window_size
    gc.collect()
    self.speech_dataset = self.get_longest_fragment(sample_window)
    self.dataset = None   # Optimize memory
    gc.collect()

    if self.speech_dataset is None or self.speech_dataset.size == 0:
      raise Exception("No suitable speech fragment block found.")

    self.speech_dataset = self.get_speech_only_from_speech_dataset()

    if self.speech_dataset is None or self.speech_dataset.size == 0:
      raise Exception("Couldn't correctly optimize speech fragment.")

    gc.collect()

    # Amplify speech fragment
    self.uniform_speech_data()

    self.check_speech_length()

  def save(self, output_file=None):
    logger.info("Saving to .wav: ")
    self.create_output_dir()

    if output_file is None:
      output_file = f'{self.title}_preek{self.file_format}'

    # Add a dir path if not already present
    if not os.path.dirname(output_file):
      output_file = os.path.join(self.output_dir, output_file)

    logger.info(" - Saving to file '%s'", output_file)
    sf.write(output_file, self.speech_dataset, self.samplerate)

    self.speech_dataset = None    # For memory issues
    gc.collect()

    self.output_files.append(output_file)

    return output_file

  def save_mp3(self, output_file=None):
    logger.info("Saving to .mp3: ")
    if output_file:
      output_file_name, output_file_ext = os.path.splitext(output_file)
    else:
      output_file_name = self.title + "_preek"

    logger.info(" - Saving .wav file first")
    output_file_wav = f"{output_file_name}_{self.get_random_string()}.tmp.wav"
    output_file_wav = self.save(output_file_wav)

    output_file_mp3 = output_file_name + ".mp3"

    # Add a dir path if not already present
    if not os.path.dirname(output_file_mp3):
      output_file_mp3 = os.path.join(self.output_dir, output_file_mp3)

    logger.info(" - Converting .wav file to .mp3: '%s'", output_file_mp3)

    gc.collect()
    fileConverter.convert_wavfile_to_mp3file(output_file_wav, output_file_mp3)
    self.output_files.append(output_file_mp3)

    logger.debug(" - Removing .wav file")
    os.remove(output_file_wav)
    self.output_files.remove(output_file_wav)

    return output_file_mp3

  def get_random_string(self, length=8):
    import random, string
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

  def get_longest_fragment(self, sample_window):
    logger.info("Getting longest audio fragment:")
    logger.info(" - Calculating average and uniform data")
    average_abs_data = self.get_average(self.dataset, sample_window, absolute=True)

    average_abs_data = self.uniform_data(average_abs_data)

    logger.info(" - Calculating data blocks")
    blocks_data = self.split_data_to_blocks(average_abs_data, split_level=self.sound_difference_threshold)

    logger.info(" - Calculating longest data block")
    longest_block = blocks_data[1] if len(blocks_data) > 0 else blocks_data[0]
    for i, block in enumerate(blocks_data):
      # Ignore first and last block (for recordings with a long silent time at the beginning or end)
      if i == 0 or i == len(blocks_data) - 1:
        continue

      if block.length() > longest_block.length():
        longest_block = block

    if self.enable_plots:
      total_minutes = len(average_abs_data) * self.sampling_window_size / 60

      # Get data for plot only
      blocks_graph_data = []
      for block in blocks_data:
        blocks_graph_data += [block.avg_amplitude()] * len(block.datapoints)

      scale_factor = total_minutes / len(blocks_graph_data)
      longest_block_start_x = longest_block.start_point * scale_factor + 0.5
      longest_block_stop_x = (longest_block.start_point + longest_block.length()) * scale_factor + 0.5

      logger.info(" - Plotting data")
      plt.figure()

      # # Plot unmodified signal. This requires a lot of memory!
      # plt.plot(np.linspace(0, len(self.dataset) / self.samplerate, len(self.dataset)), self.dataset)
      # plt.grid()
      # plt.ylim(-1, 1)
      # plt.xlabel('Time (s)')
      # plt.ylabel('Amplitude')
      # plt.title("Unmodified")

      plt.subplot(211)
      plt.plot(np.linspace(0, total_minutes, len(average_abs_data)), average_abs_data)
      plt.axvspan(longest_block_start_x, longest_block_stop_x, color='blue', alpha=0.1)
      plt.grid()
      plt.xlim(0, total_minutes)
      plt.ylim(0, 1)
      plt.xlabel('Time (minutes)')
      plt.ylabel('Amplitude')
      plt.title("Absolute (amplified)")

      plt.subplot(212)
      plt.plot(np.linspace(0, total_minutes, len(blocks_graph_data)), blocks_graph_data)
      plt.axvspan(longest_block_start_x, longest_block_stop_x, color='blue', alpha=0.1)
      plt.grid()
      plt.xlim(0, total_minutes)
      plt.ylim(0, 1)
      plt.xlabel('Time (minutes)')
      plt.ylabel('Amplitude')
      plt.title("Blocks of absolute (amplified)")

      plt.suptitle("Sample window: {} s".format(sample_window / self.samplerate))
      plt.show()

      if self.save_plots:
        logger.info(" - Plot title: %s", self.title)
        output_file_plot = f"{self.output_dir}/{self.title}_plot_blocks_{self.get_random_string(4)}.png"
        logger.info(" - Saving plot to '%s'", output_file_plot)
        plt.savefig(output_file_plot, bbox_inches='tight')
        self.output_files.append(output_file_plot)

      plt.close()

    logger.info(" - Extracting longest fragment")
    return self.project_block_on_dataset(longest_block, self.dataset, sample_window)

  def get_average(self, dataset, sample_window, absolute=True):
    logger.info("Calculating average: ")
    average_sig = []
    previous_process_value = 0
    dataset_length = len(dataset)
    for i in range(0, dataset_length, sample_window):
      current_process_value = i // (dataset_length / 100)
      if current_process_value != previous_process_value:
        previous_process_value = current_process_value
        logger.debug(" - %s%%", current_process_value)

      start_index = i
      stop_index = i + sample_window
      if stop_index > dataset_length:
        stop_index = dataset_length

      sample = dataset[start_index:stop_index]
      if absolute:
        sample = np.abs(sample)

      average = np.mean(sample)
      average_sig.append(average)
    return average_sig

  def uniform_data(self, dataset):
    logger.info("Uniforming data")
    return dataset / np.abs(dataset).max()

  def uniform_speech_data(self):
    logger.info("Uniforming speech data")
    max_value = np.max(self.speech_dataset)
    self.speech_dataset /= max_value

  def split_data_to_blocks(self, dataset, split_level):
    logger.info("Splitting data to blocks: ")
    blocks = []
    block = Block(0, dataset[0])
    blocks.append(block)

    previous_process_value = 0
    dataset_length = len(dataset)
    for i, datapoint in enumerate(dataset):
      current_process_value = i // (dataset_length / 100)
      if current_process_value != previous_process_value:
        previous_process_value = current_process_value
        logger.debug(" - %s%%", current_process_value)

      lastblock = blocks[-1]

      if abs(lastblock.avg_amplitude() - datapoint) > split_level:
        # Create new block
        lastblock.end_at(datapoint)
        newblock = Block(i, datapoint)
        blocks.append(newblock)
      else:
        # Add point to block
        lastblock.datapoints.append(datapoint)
    return blocks

  def project_block_on_dataset(self, block, dataset, block_sample_window):
    start_point = block.start_point * block_sample_window
    stop_point = start_point + block.length() * block_sample_window
    return dataset[start_point:stop_point]

  def get_speech_only_from_speech_dataset(self):
    logger.info("Getting speech only from speech dataset: ")

    # Remove music from begin and end
    logger.info(" - Removing music from beginning and end")
    speek_fragment_start = self.find_first_silence_index(self.speech_dataset, self.samplerate * 0.5, 5)
    speek_fragment_stop = len(self.speech_dataset) - self.find_first_silence_index(self.speech_dataset[::-1],
                                                                                   sample_window=self.samplerate * 0.5,
                                                                                   min_required_matches=5)

    self.speech_dataset = self.speech_dataset[speek_fragment_start:speek_fragment_stop]

    # Remove silence from begin and end
    logger.info(" - Removing silence from beginning and end")
    speek_fragment_start = self.cut_to_first_sound(self.speech_dataset, sample_window=self.samplerate * 0.4)
    speek_fragment_stop = len(self.speech_dataset) - self.cut_to_first_sound(self.speech_dataset[::-1],
                                                                             sample_window=self.samplerate * 0.05)

    # Add padding to begin and end
    logger.info(" - Adding some padding to beginning and end")
    if (speek_fragment_start - self.samplerate * self.output_begin_padding) < 0:
      speek_fragment_start = 0
    else:
      speek_fragment_start -= self.samplerate * self.output_begin_padding

    if (speek_fragment_stop + self.samplerate * self.output_end_padding) > len(self.speech_dataset):
      speek_fragment_stop = len(self.speech_dataset)
    else:
      speek_fragment_stop += self.samplerate * self.output_end_padding

    self.speech_dataset = self.speech_dataset[speek_fragment_start:speek_fragment_stop]

    if self.enable_plots:
      logger.info(" - Plotting data")
      plt.figure()
      plt.subplot(211)
      plt.title("First 10 seconds of speech")
      plt.grid()
      plt.xlim(0, 10)
      plt.ylim(-1, 1)
      plt.xlabel('Time (s)')
      plt.ylabel('Amplitude')
      plt.plot(np.linspace(0, 10, self.samplerate * 10), self.speech_dataset[0:(self.samplerate * 10)])
      plt.subplot(212)
      plt.title("Last 10 seconds of speech")
      plt.grid()
      plt.xlim(-10, 0)
      plt.ylim(-1, 1)
      plt.xlabel('Time (s)')
      plt.ylabel('Amplitude')
      plt.plot(np.linspace(-10, 0, self.samplerate * 10), self.speech_dataset[(self.samplerate * -10):])
      plt.show()

      if self.save_plots:
        output_file_plot = f"{self.output_dir}/{self.title}_plot_tails_{self.get_random_string(4)}.png"
        logger.info(" - Saving plot to '%s'", output_file_plot)
        plt.savefig(output_file_plot, bbox_inches='tight')
        self.output_files.append(output_file_plot)

      plt.close()

    return self.speech_dataset

  def find_first_silence_index(self, dataset, sample_window, min_required_matches, silence_threshold=None):
    logger.info("Finding first silence")
    sample_window = int(sample_window)

    if silence_threshold is None:
      silence_threshold = self.first_silence_threshold

    matcheseries_found = 0
    for i in range(0, len(dataset), sample_window):
      # Get subset
      start_index = i
      stop_index = i + sample_window
      if stop_index > len(dataset):
        stop_index = len(dataset)
      sample = dataset[start_index:stop_index]
      sample = np.abs(sample)

      # Get average
      average = np.mean(sample)

      # Compare
      if average < silence_threshold:
        matcheseries_found += 1

        if matcheseries_found > min_required_matches:
          return i
      else:
        matcheseries_found = 0
    return 0

  def cut_to_first_sound(self, dataset, sample_window, sound_threshold=None):
    logger.info("Finding first sound")
    sample_window = int(sample_window)

    if sound_threshold is None:
      sound_threshold = self.first_sound_threshold

    for i in range(0, len(dataset), sample_window):
      # Get subset
      start_index = i
      stop_index = i + sample_window
      if stop_index > len(dataset):
        stop_index = len(dataset)
      sample = dataset[start_index:stop_index]
      sample = np.abs(sample)

      # Get average
      average = np.mean(sample)

      if average >= sound_threshold:
        return i

    return 0

  def check_speech_length(self):
    total_seconds = int(len(self.speech_dataset) / self.samplerate)
    hours = total_seconds // 3600
    minutes = total_seconds // 60 - hours * 60
    seconds = total_seconds - minutes * 60 - hours * 3600

    logger.info("Speech length: %s hours, %s minutes, %s seconds", hours, minutes, seconds)
    if total_seconds < self.min_speech_length:
      logger.warning("WARNING: speech length is too short")

  def create_output_dir(self):
    if os.path.exists(self.output_dir):
      return True

    logger.info("Creating direcotry %s",  self.output_dir)
    try:
      os.makedirs(self.output_dir)
      return True
    except OSError as e:
      logger.error("Creation of the directory '%s' failed", self.output_dir)
      raise e


class Block:
  start_point = None
  datapoints = []

  def __init__(self, arg_start_point=None, arg_start_point_value=None):
    self.start_point = arg_start_point
    self.datapoints = [arg_start_point_value]

  def avg_amplitude(self):
    return np.mean(self.datapoints)

  def end_at(self, datapoint):
    if datapoint not in self.datapoints:
      self.datapoints.append(datapoint)

  def length(self):
    return len(self.datapoints)
