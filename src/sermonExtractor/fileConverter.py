import logging
import subprocess

from pydub.exceptions import CouldntDecodeError

logger = logging.getLogger(__name__)


def convert_wavfile_to_mp3file(input_wav_file_path, output_mp3_file_path):
  logger.info("Converting %s to %s...", input_wav_file_path, output_mp3_file_path)
  p = subprocess.Popen(['ffmpeg', '-y', '-f', 'wav', '-i', input_wav_file_path, '-f', 'mp3', output_mp3_file_path],
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  p_out, p_err = p.communicate()

  logger.debug("Process std_out: %s", p_out)
  logger.debug("Process std_err: %s", p_err)

  if p.returncode != 0:  # or len(p_out) == 0:
    raise CouldntDecodeError(
      "Decoding failed. ffmpeg returned error code: {0}\n\nOutput from ffmpeg/avlib:\n\n{1}".format(p.returncode,
                                                                                                    p_err))
  logger.info("Converting done.")


def convert_mp3file_to_wavfile(input_mp3_file_path, output_wav_file_path):
  logger.info("Converting %s to %s...", input_mp3_file_path, output_wav_file_path)
  p = subprocess.Popen(
    ['ffmpeg', '-y', '-f', 'mp3', '-i', input_mp3_file_path, '-acodec', 'pcm_s16le', '-vn', '-f', 'wav',
     output_wav_file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  p_out, p_err = p.communicate()

  logger.debug("Process std_out: %s", p_out)
  logger.debug("Process std_err: %s", p_err)

  if p.returncode != 0:  # or len(p_out) == 0:
    raise CouldntDecodeError(
      "Decoding failed. ffmpeg returned error code: {0}\n\nOutput from ffmpeg/avlib:\n\n{1}".format(p.returncode,
                                                                                                    p_err))
  logger.info("Converting done.")
