#!/usr/bin/env python
# First:
# $ sudo apt install ffmpeg
# $ pip install -r requirements
#
# Alternatively:
# $ sudo apt install mpg123
# $ mpg123 -w output_file.wav input_file.mp3

import getopt
import logging
import resource
import sys
import os

import gc
from sermonExtractor import fileConverter

from sermonExtractor.sermon_extractor import SermonExtractor

logger = logging.getLogger(__name__)

valid_output_formats = ('wav', 'mp3')


# def memory_limit(new_limit):
#   soft, hard = resource.getrlimit(resource.RLIMIT_AS)
#   resource.setrlimit(resource.RLIMIT_AS, (new_limit * 1024, hard))
#
#
# def get_memory():
#   with open('/proc/meminfo', 'r') as mem:
#     free_memory = 0
#     for i in mem:
#       sline = i.split()
#       if str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
#         free_memory += int(sline[1])
#   print(free_memory)


def usage(exit_code=0):
  if __name__ != "__main__" and exit_code != 0:
    raise Exception("Invalid usage of sermonExtractorMain")

  print(f"""Extract the longest speech part of a sermon audio file.

Usage:  sermonExtractorMain.py OPTION
With OPTION as one or more of the follows:
    -h, --help                      Shows this usage message.
    -i, --input_file=FILE            The sermon audio file (.wav) to extract the speech part from.
    -f --output_format=FORMAT      The file format of the output audio file. Choices are: 
                                      {', '.join(valid_output_formats)}. 
                                      Defaults to the input file format.
    -d, --output_dir=PATH           All output files will be saved to this directory. Output file will only be saved 
                                      to this directory, if a directory is not specified in its own path.
    -o, --output_file=FILE           The output audio file (.wav) to be saved to. If not given, the input 
                                      file + "_output" will be used as name.
    -v, --enable_plots              Show matplotlib plots while converting.
        --save_plots                Save plots as an image. Note that --enable_plots must be turned on.
        --sound_difference_threshold=TRESHOLD       Sound difference for detecting different audio parts. 
    """)
  sys.exit(exit_code)


def main(argv):
  # Input collection
  try:
    opts, args = getopt.getopt(argv, "hi:o:d:vf:", ["help", "input_file=", "output_file=", "enable_plots",
                                                    "sound_difference_threshold=", "output_format=", "save_plots"])
  except getopt.GetoptError:
    usage(2)

  input_file = None
  output_file = None
  output_dir = None
  enable_plots = False
  sound_difference_threshold = None
  output_format = None
  save_plots = False

  for opt, arg in opts:
    if opt in ('-h', '--help'):
      usage()
    elif opt in ('-i', '--input_file'):
      input_file = arg
    elif opt in ('-o', '--output_file'):
      output_file = arg
    elif opt in ('-d', '--output_dir'):
      output_dir = arg
    elif opt in ('-v', '--enable_plots'):
      enable_plots = True
    elif opt == '--sound_difference_threshold':
      sound_difference_threshold = float(arg)
    elif opt in ('-f', '--output_format'):
      output_format = arg
    elif opt == '--save_plots':
      save_plots = True

  output_files = execute(input_file, output_file, output_dir, enable_plots, sound_difference_threshold, output_format,
                         save_plots)

  print("Output files: {}".format(', '.join(output_files)))


# Returns all the files which has been created during processing, like plots and the output audio.
def execute(input_file_path=None,
            output_file_name=None,
            output_dir=None,
            enable_plots=False,
            sound_difference_threshold=None,
            output_format=None,
            save_plots=False,
            increase_progress_func=None):
  if not increase_progress_func:
    increase_progress_func = update_progress_stub

  # Validation
  if input_file_path is None:
    raise ValueError("Missing option 'input_file'!\n")
  ifile_name, ifile_ext = os.path.splitext(input_file_path)

  if output_format is None:
    output_format = ifile_ext[1:]

  if output_format not in valid_output_formats:
    raise ValueError(f"Invalid output_format '{output_format}'!\n")

  if output_dir is None:
    # Get output dir from output file, if a path is set.
    if output_file_name:
      if os.path.dirname(output_file_name):
        output_dir = os.path.dirname(output_file_name)
  increase_progress_func(message="- Omzetten van audiobestand...")

  # Check for .wav extenstion
  input_file_title = os.path.basename(ifile_name)
  if ifile_ext != ".wav":
    import random
    randomstring = get_random_string()
    input_file_wav = f"{ifile_name}_{randomstring}.tmp.wav"
    fileConverter.convert_mp3file_to_wavfile(input_file_path, input_file_wav)
    input_file_path = input_file_wav

    # Free memory
    gc.collect()
  increase_progress_func()

  # Set the settings
  sermon_extractor = SermonExtractor()
  sermon_extractor.enable_plots = enable_plots
  sermon_extractor.save_plots = save_plots
  if sound_difference_threshold is not None:
    sermon_extractor.sound_difference_threshold = sound_difference_threshold
  if output_dir is not None:
    sermon_extractor.output_dir = output_dir
  increase_progress_func()

  # Do the processing
  try:
    sermon_extractor.read(input_file_path)
    sermon_extractor.title = input_file_title
    increase_progress_func(message="- Preek ophalen uit audio bestand...")
    sermon_extractor.convert()
    increase_progress_func(message="- Preek opslaan...")

    # Free memory
    gc.collect()

    if output_format == 'mp3':
      sermon_extractor.save_mp3(output_file_name)
    else:
      sermon_extractor.save(output_file_name)
    increase_progress_func(message="- Opruimen...")
  except Exception as exception:
    logger.error("Something went wrong. The file could not be processed.")
    logger.exception(exception, exc_info=True)
    raise
  finally:
    # Clean up after use
    if ifile_ext != ".wav":
      os.remove(input_file_path)
    increase_progress_func(message="- Klaar met preek filteren uit audio bestand.")

  return sermon_extractor.output_files


def get_random_string(length=8):
  import random, string
  letters = string.ascii_lowercase
  return ''.join(random.choice(letters) for i in range(length))


def update_progress_stub(message=None):
  if message:
    logger.info(message)


if len(sys.argv) == 0:
  raise Exception("No input file specified")

if __name__ == "__main__":
  # memory_limit(1527428)     # Set memory limit for local testing
  logging.basicConfig(
    format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
    level=logging.DEBUG)

  try:
    main(sys.argv[1:])
  except ValueError as e:
    logger.exception(e, exc_info=True)
    usage(1)
